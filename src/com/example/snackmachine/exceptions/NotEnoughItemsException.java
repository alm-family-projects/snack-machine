package com.example.snackmachine.exceptions;

public class NotEnoughItemsException extends RuntimeException {
	private static final long serialVersionUID = -7607413619780305717L;
	private String message;
    
    public NotEnoughItemsException(String string) {
        this.message = string;
    }
    
    @Override
    public String getMessage(){
    	return this.message;
    }
}
