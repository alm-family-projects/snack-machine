package com.example.snackmachine.exceptions;

public class NotSufficientChangeException extends RuntimeException {
	private static final long serialVersionUID = 2451833452562654476L;
	private String message;
    
    public NotSufficientChangeException(String string) {
        this.message = string;
    }
    
    @Override
    public String getMessage(){
    	return this.message;
    } 
}
