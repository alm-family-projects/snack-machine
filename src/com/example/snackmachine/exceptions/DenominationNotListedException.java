package com.example.snackmachine.exceptions;

public class DenominationNotListedException extends RuntimeException {
	private static final long serialVersionUID = -3691863295228940950L;
	private String message;
    
    public DenominationNotListedException(String message) {
    	this.message = message;
    }
    
    @Override
    public String getMessage() {
    	return this.message;
    }
    
}
