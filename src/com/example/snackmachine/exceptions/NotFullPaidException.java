package com.example.snackmachine.exceptions;

public class NotFullPaidException extends RuntimeException {
	private static final long serialVersionUID = -2226912452413970777L;
	private String message;
    private float remaining;
    
    public NotFullPaidException(String message, float remainingBalance) {
    	this.message = message;
    	this.remaining = remainingBalance;
    }
    
    public float getRemaining() {
    	return remaining;
    }
    
    @Override
    public String getMessage() {
    	return this.message;
    }
}
