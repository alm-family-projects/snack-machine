package com.example.snackmachine.exceptions;

public class SoldOutException extends RuntimeException {
	private static final long serialVersionUID = -4066744939447329164L;
	private String message;
    
    public SoldOutException(String string) {
        this.message = string;
    }
    
    @Override
    public String getMessage(){
    	return this.message;
    }
}
