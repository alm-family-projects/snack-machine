package com.example.snackmachine;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

import com.example.snackmachine.exceptions.DenominationNotListedException;
import com.example.snackmachine.exceptions.NotEnoughItemsException;
import com.example.snackmachine.exceptions.NotFullPaidException;
import com.example.snackmachine.exceptions.NotSufficientChangeException;
import com.example.snackmachine.exceptions.SoldOutException;

public class SnackMachine {
	private Inventory<Item> itemInventory = new Inventory<Item>();
    private Inventory<Coin> cashInventory = new Inventory<Coin>();
    private float moneyBalance = 0;
    private float currentBalance = 0;
    private Integer currentQuantity;
    private Item currentItem;
    private String errorMessage = "";
    private String labelMessage = "";
    
    public SnackMachine() {
    	initialize();
    	showMenu();
    }
    
    private void initialize() {
    	// Initialize the machine with 10 items of each type
    	for (Item i: Item.values()) {
    		itemInventory.put(i, 10);
    	}
    	
    	// Initialize the machine with 10 * �1
    	cashInventory.put(Coin.ONEPOUND, 10);
    	
    	// Initialize the machine with 20 * �0.50
    	cashInventory.put(Coin.FIFTYPENCE, 20);
    	
    	// Initialize the machine with 20 * �0.20
    	cashInventory.put(Coin.TWEENTYPENCE, 20);
    	
    	// Initialize the machine with 20 * �0.10
    	cashInventory.put(Coin.TENPENCE, 20);
    	
    	// Initialize the machine with 20 * �0.05
    	cashInventory.put(Coin.FIVEPENCE, 20);
    	
    	// Calculate the initial total profit / losses
    	for (Item i: Item.values()) {
    		moneyBalance -= itemInventory.getQuantity(i) * 0.45f;	
    	}
    }
    
    @SuppressWarnings("resource")
	private String readInput() {
        return new Scanner(System.in).nextLine();
    }
    
    public float totalMoney() {
    	float totalMoney = 0;
    	
    	for (Coin c: Coin.values()) {
    		totalMoney += cashInventory.getQuantity(c) * c.getDenomination();
    	}
    	return totalMoney;
    }
    
    public float moneyBalance() {
    	return moneyBalance;
    }
    
    public void selectItem(Item item) {
    	if (itemInventory.hasItem(item)) {
    		currentItem = item;
    		return;
    	}
    	throw new SoldOutException("Sold out, please select a different item!");
    }
    
    public void selectQuantity(Integer quantity) {
    	int itemQuantity = itemInventory.getQuantity(currentItem);
    	
    	if (itemQuantity >= quantity) {
    	    currentQuantity = quantity;
    	    return;
        }
    	
    	throw new NotEnoughItemsException("There are only " + itemQuantity + " items left!");
    }
    
    public void insertCoin(float coin) {
        for (Coin c: Coin.values()) {
        	if (c.getDenomination() == coin) {
        		currentBalance += c.getDenomination();
        		cashInventory.add(c);
        		return;
        	}
        }
        throw new DenominationNotListedException("The given coin is not accepted by the machine!");
    }

    private Item collectItem() {
    	if (isFullPaid()) {
    		if (hasSufficientChange()) {
    			for (int i = 0; i < currentQuantity; i++) {
        	    	itemInventory.deduct(currentItem);
        	    }
        	    return currentItem;
    		}	
    		throw new NotSufficientChangeException("Not sufficient change in inventory");
    	}
    	float remainingBalance = currentItem.getPrice() * currentQuantity - currentBalance;
    	throw new NotFullPaidException("Price not full paid, the transaction was declined", remainingBalance);
    }
    
    private List<Coin> collectChange() {
    	float changeAmount = currentBalance - currentItem.getPrice() * currentQuantity;
    	List<Coin> change = getChange(changeAmount);
    	updateCashInventory(change);
    	moneyBalance += currentItem.getPrice() * currentQuantity;
    	currentItem = null;
    	currentQuantity = null;
    	currentBalance = 0;
    	return change;
    }
    
    public List<Coin> refund() {
    	List<Coin> refund = getChange(currentBalance);
    	updateCashInventory(refund);
    	currentItem = null;
    	currentQuantity = null;
    	currentBalance = 0;
    	return refund;
    }
    
    private void showChange(List<Coin> change) {
    	if (change.size() > 0) {
    		labelMessage = "Change: ";
        	for (int i = 0; i < change.size(); i++) {
        		if (i == change.size() - 1) {
        			labelMessage += String.format("�%.2f\n", change.get(i).getDenomination());
        		} else {
        			labelMessage += String.format("�%.2f, ", change.get(i).getDenomination());
        		}
        	}
    	}
    }
    
    private void showRefund(List<Coin> refund) {
    	if (refund.size() > 0) {
    		labelMessage = "Refund: ";
        	for (int i = 0; i < refund.size(); i++) {
        		if (i == refund.size() - 1) {
        			labelMessage += String.format("�%.2f\n", refund.get(i).getDenomination());
        		} else {
        			labelMessage += String.format("�%.2f, ", refund.get(i).getDenomination());
        		}
        	}
    	}
    }
    
    private boolean isFullPaid() {
        if (currentBalance >= currentItem.getPrice() * currentQuantity) {
        	return true;
        }
        return false;
    }
   
    private List<Coin> getChange(float amount) {
    	List<Coin> changes = Collections.emptyList();
    	
    	if(amount > 0) {
    		changes = new ArrayList<Coin>();
    		float balance = amount;
    		
    		while (balance > 0) {
    			balance = (float) (Math.round(balance*100.0)/100.0);
    			
    			if (balance >= Coin.ONEPOUND.getDenomination()
    			             && cashInventory.hasItem(Coin.ONEPOUND)) {
    				changes.add(Coin.ONEPOUND);
    				balance -= Coin.ONEPOUND.getDenomination();
    				continue;
    			} else if (balance >= Coin.FIFTYPENCE.getDenomination() 
    					           && cashInventory.hasItem(Coin.FIFTYPENCE)) {
    				changes.add(Coin.FIFTYPENCE);
    				balance -= Coin.FIFTYPENCE.getDenomination();
    				continue;
    			} else if (balance >= Coin.TWEENTYPENCE.getDenomination() 
    					           && cashInventory.hasItem(Coin.TWEENTYPENCE)) {
    				changes.add(Coin.TWEENTYPENCE);
    				balance -= Coin.TWEENTYPENCE.getDenomination();
    				continue;
    			} else if (balance >= Coin.TENPENCE.getDenomination()
    					           && cashInventory.hasItem(Coin.TENPENCE)) {
    				changes.add(Coin.TENPENCE);
    				balance -= Coin.TENPENCE.getDenomination();
    				continue;
    			} else if (balance >= Coin.FIVEPENCE.getDenomination()
    					           && cashInventory.hasItem(Coin.FIVEPENCE)) {
    				changes.add(Coin.FIVEPENCE);
    				balance -= Coin.FIVEPENCE.getDenomination();
    				continue;
    			} else {
    				throw new NotSufficientChangeException("Not enough change, please try another product");
    			}
    		}
    	}
    	
    	return changes;
    }
    
    private boolean hasSufficientChange() {
    	return hasSufficientChangeForAmount(currentBalance - currentItem.getPrice() * currentQuantity);
    }
    
    private boolean hasSufficientChangeForAmount(float amount) {
        boolean hasChange = true;
        
        try {
        	getChange(amount);
        } catch(NotSufficientChangeException e){
           hasChange = false;
        }
        
        return hasChange;
    }
    
    private void updateCashInventory(List<Coin> change) {
        for (Coin c: change) {
        	cashInventory.deduct(c);
        }
    }

    public void showMenu() {
    	String option;
    	
    	while (true) {
    		int nr = 1;
    		
    		System.out.println("");
    		System.out.format("%28s\n\n", "Snack Machine");
        	System.out.format("%5s%-20s %-5s %6s\n"," ", "SNACK", "PRICE", "QTY");
        	System.out.println(String.format("%42s", " ").replace(" ", "-"));
        	
        	// Print menu items
        	for (Item i : Item.values()) {
        		if (itemInventory.getQuantity(i) == 0) {
        			System.out.format("%3d. %-20s �%-5.2f %18s\n", nr++, i.getName(), i.getPrice(), "***out of stock***");
        		} else {
        			System.out.format("%3d. %-20s �%-5.2f %5d\n", nr++, i.getName(), i.getPrice(), itemInventory.getQuantity(i));
        		}
        	}
        	
        	
    		if (errorMessage.length() > 0) {
    			System.out.println("\nERROR: " + errorMessage);
    			if (labelMessage.length() > 0) {
        			System.out.print(labelMessage);
        			labelMessage = "";
    			}
    			System.out.print("Please enter an option ( 0 to exit ): ");
    			errorMessage = "";
    		} else if (labelMessage.length() > 0) {
    			System.out.print("\n" + labelMessage);
    			labelMessage = "";
    			System.out.print("Please enter an option ( 0 to exit ): ");
    		} else {
    			System.out.print("\nPlease enter an option ( 0 to exit ): ");
    		}
    		
    		option = readInput();
    		
    		try {
        		switch(option) {
    	            case "0":
    	    	        return;
    	            case "1":
    	    	        selectItem(Item.CRISPS);
    	    	        showSubMenu();
    	    	        showPaymentMenu();
    	    	        break;
    	            case "2":
    	    	        selectItem(Item.MARSBAR);
    	    	        showSubMenu();
    	    	        showPaymentMenu();
    	    	        break;
    	            case "3":
    	    	        selectItem(Item.COCACOLA);
    	    	        showSubMenu();
    	    	        showPaymentMenu();
    	    	        break;
    	            case "4":
    	    	        selectItem(Item.EUGENIA);
    	    	        showSubMenu();
    	    	        showPaymentMenu();
    	    	        break;
    	            case "5":
    	    	        selectItem(Item.WATER);
    	    	        showSubMenu();
    	    	        showPaymentMenu();
    	    	        break;
    	            case "10976":
    	        	    showAdminMenu();
    	    	        break;
    	            default:
    	            	errorMessage = "You must enter an option between 0-5 ( 0 for exit)";
    	        }
        	} catch (SoldOutException e) {
        	    errorMessage = e.getMessage();
        	}
    	}
    }
    
    public void showSubMenu() {
    	String option;
    	int quantity;
    	
    	while (true) {
    		System.out.println("");
    		System.out.format("%25s\n\n", "Quantity selection");
    		System.out.println(String.format("%42s", " ").replace(" ", "-"));
    		
    		if (errorMessage.length() > 0) {
    			System.out.println("\nERROR: " + errorMessage);
    			System.out.print("Please enter the quantity: ");
    			errorMessage = "";
    		} else {
    			System.out.format("The current available quantity for '%s' is %d", currentItem.getName(), itemInventory.getQuantity(currentItem));
    			System.out.print("\nPlease enter the quantity: ");
    		}
    		
    		try {
    			option = readInput();
    			quantity = Integer.parseInt(option);
    			
    			if (quantity > 0) {
       	    		selectQuantity(quantity);
       	    		return;
       	    	} else {
       	    		throw new NumberFormatException();
       	    	}
    		} catch (NotEnoughItemsException e) {
       	    	errorMessage = e.getMessage();
       	    } catch (NumberFormatException e) {
       	    	errorMessage = "The quantity value should be between 1 and 10";
       	    }
    	}
    }
    
    public void showPaymentMenu() {
    	String option;
    	float remainingBalance = currentItem.getPrice() * currentQuantity - currentBalance;
    	
    	while (true) {
    		System.out.println("");
    		System.out.format("%25s\n\n", "Payment");
    		System.out.println(String.format("%42s", " ").replace(" ", "-"));
    		
    		if (errorMessage.length() > 0) {
    			System.out.println("\nERROR: " + errorMessage);
    			System.out.format("Current remaining balance: %.2f\n", remainingBalance);
    			System.out.print("Please enter a coin ( 0 to refund ): ");
    			errorMessage = "";
    		} else {
    			System.out.format("\nCurrent remaining balance: %.2f\n", remainingBalance);
    			System.out.print("Please enter a coin ( 0 to refund ): ");
    		}
    		
    		try {
    			option = readInput();
    		    
    			if (option.equals("0")) {
    				collectItem();
    				showChange(collectChange());
    				return;
    			}
    			
    			insertCoin(Float.parseFloat(option));
    			remainingBalance = currentItem.getPrice() * currentQuantity - currentBalance;
    			
    			if (remainingBalance <= 0) {
    				collectItem();
    				showChange(collectChange());
    				return;
    			}
    			
    		} catch (NumberFormatException e) {
    			errorMessage = "The given coin is not accepted by the machine!";
    		} catch (NotSufficientChangeException e) {
    			errorMessage = e.getMessage();
    			showRefund(refund());
    			return;
    		} catch (NotFullPaidException e) {
    			errorMessage = e.getMessage();
    			showRefund(refund());
    			return;
    		} catch (DenominationNotListedException e) {
    			errorMessage = e.getMessage();
    		}
    	}
    }
    
    public void showAdminMenu() {
    	String option;
    	String result = "";
    	
    	while (true) {
    		System.out.println("");
    		System.out.format("%25s\n\n", "Admin Menu");
    		System.out.println(String.format("%42s", " ").replace(" ", "-"));
    		
    		System.out.format("1. %-25s\n", "Total profit / losses");
    		System.out.format("2. %-25s\n", "Total money in the machine");
    		
    		System.out.print("\n" + result);
    		
    		if (errorMessage.length() > 0) {
    			System.out.println("\nERROR: " + errorMessage);
    			System.out.print("Please enter an option ( 0 to return ): ");
    			errorMessage = "";
    		} else {
    			System.out.print("Please enter an option ( 0 to return ): ");
    		}
    		
    		option = readInput();
    		
        	switch(option) {
    	        case "0":
    	    	   return;
    	        case "1":
    	           result = String.format("INFO: Total profit / losses are �%.2f\n", moneyBalance);	 
    	    	   break;
    	        case "2":
    	           result = String.format("INFO: There are �%.2f in the machine\n", this.totalMoney()); 	
    	    	   break;
    	        default:
    	           errorMessage = "You must enter an option between 0-2";
    	           result = "";
        	}  
    	}
    }
    
    public void reset() {
    	cashInventory.clear();
    	itemInventory.clear();
    	currentItem = null;
    	currentQuantity = null;
    	currentBalance = 0;
    }
}
