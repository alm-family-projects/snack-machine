package com.example.snackmachine;

public enum Coin {
	FIVEPENCE(0.05f),
	TENPENCE(0.10f),
	TWEENTYPENCE(0.20f),
	FIFTYPENCE(0.50f),
	ONEPOUND(1f);
	
    private float denomination;
	
	private Coin(float denomination) {
		this.denomination = denomination;
	}
	
	public float getDenomination() {
		return denomination;
	}
}
