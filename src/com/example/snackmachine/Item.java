package com.example.snackmachine;

public enum Item {
	CRISPS("Crisps", 0.75f),
	MARSBAR("Mars Bar", 0.70f),
	COCACOLA("Coca Cola zero", 1f),
	EUGENIA("Eugenia", 0.50f),
	WATER("Water", 0.85f);
	
    private String name;
	private float price;
	
	private Item(String name, float price) {
		this.name = name;
		this.price = price;
	}
	
	public String getName() {
		return name;
	}
	
	public float getPrice() {
		return price;
	}
}
